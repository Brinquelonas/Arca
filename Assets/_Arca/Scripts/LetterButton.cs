﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterButton : MonoBehaviour {

    public Action OnClick;
    public Action OnUp;
    public Action OnDown;

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    void OnMouseUpAsButton()
    {
        OnClick();
    }

    void OnMouseUp()
    {
        OnUp();
    }

    void OnMouseDown()
    {
        OnDown();
    }
}
